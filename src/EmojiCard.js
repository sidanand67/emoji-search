import React from 'react'; 
import './EmojiCard.css'; 

const EmojiCard = ({symbol, title}) => {
    return (
        <div
            className="card"
            onClick={() => navigator.clipboard.writeText(symbol)}
        >
            <p className="symbol">
                {symbol}    
                <span className="title">{title}</span>
            </p>
            <p className="copy-msg">Click to Copy</p>
        </div>
    );
}

export default EmojiCard; 