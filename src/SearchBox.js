import React from 'react'; 
import './SearchBox.css'; 

const SearchBox = ({onInputChange}) => {
    return (
        <input type="text" placeholder="Search for emoji here..." onChange={onInputChange}/> 
    ); 
}

export default SearchBox; 