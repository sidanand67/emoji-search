import React, { Component } from 'react'; 
import Header from './Header'; 
import './App.css'; 
import EmojiList from './emojiList.json'; 
import EmojiCardList from './EmojiCardList'; 
import SearchBox from './SearchBox'; 
import FilterEmojis from './FilterEmojis';

class App extends Component {

    constructor(props){
        super(props); 

        this.state = {
            emojis: EmojiList, 
        }
    }

    onInputChange = (event) => {
        this.setState({emojis: FilterEmojis(event.target.value)})
    }

    render() {
        return (
            <div className="container">
                <Header />
                <SearchBox onInputChange={this.onInputChange}/> 
                <EmojiCardList emojiList={this.state.emojis}/>
            </div>
        );
    }
}

export default App; 