import React from 'react'; 
import './Header.css'; 

const Header = () => {
    return (
        <header className="header">
            <h1>
                <i className="fa-solid fa-face-grin-tongue-squint emoji-icon"></i>
                Emoji Search
                <i className="fa-solid fa-face-grin-tongue-squint emoji-icon"></i>
            </h1>
        </header>
    );
}

export default Header; 