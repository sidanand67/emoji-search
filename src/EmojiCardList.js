import React from 'react'; 
import EmojiCard from './EmojiCard'; 
import './EmojiCardList.css'; 

const EmojiCardList = ({emojiList}) => {
    const emojiCardArray = emojiList.map(emoji => {
        return (
            <EmojiCard
                key={emoji.title}
                symbol={emoji.symbol}
                title={emoji.title}
            />
        );
    }); 

    return (
        <div className="emoji-container">
            {emojiCardArray.length > 0 ? (
                emojiCardArray
            ) : (
                <p className="error">
                    <i className="fa-solid fa-xmark error-icon"></i>No matches found.
                </p>
            )}
        </div>
    ); 
}

export default EmojiCardList; 