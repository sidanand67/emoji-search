import EmojiList from './emojiList.json'

const FilterEmojis = (query) => {
    return EmojiList.filter(emoji => {
        if(emoji.title.toLowerCase().includes(query.toLowerCase()) || emoji.keywords.includes(query.toLowerCase())){
            return true; 
        }
        else {
            return false; 
        }
    }); 
}

export default FilterEmojis; 